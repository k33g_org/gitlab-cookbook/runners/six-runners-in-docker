#!/bin/bash

set -o allexport; source .env; set +o allexport

docker run --rm -it -v gitlab-runner01-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "gitpod_01" \
  --tag-list "gitpod_01" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner02-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "gitpod_02" \
  --tag-list "gitpod_02" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner03-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "gitpod_03" \
  --tag-list "gitpod_03" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner04-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "gitpod_04" \
  --tag-list "gitpod_04" \
  --run-untagged="true" \
  --locked="false"

docker run --rm -it -v gitlab-runner05-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "gitpod_05" \
  --tag-list "gitpod_05" \
  --locked="false"

docker run --rm -it -v gitlab-runner06-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_COOKBOOK_RUNNERS}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "gitpod_06" \
  --tag-list "gitpod_06" \
  --run-untagged="true" \
  --locked="false"
